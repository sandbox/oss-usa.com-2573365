<?php
/**
 * @file
 *
 * This module perform velocity checking against the IP address of the last known
 * valid log-in from user.
 *
 * Correct credentials supplied from a user originating from the United Kingdom at GMT 10:00Hrs
 * and a second log-in attempt from a user in Venezuela at GMT 10:27Hrs.
 */


/**
* Implements hook_menu().
*/
function owasp_login_velocity_check_menu() {
  $items['admin/config/system/owasp-check'] = array(
    'title' => 'OWASP check settings',
    'page callback' => 'owasp_login_velocity_check_page',
    'page arguments' => array('owasp_login_velocity_check_form'),
    'access arguments' => array('owasp check by ip'),
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function owasp_login_velocity_check_permission() {
  return array(
    'owasp check by ip' => array(
      'title' => t('OWASP check'),
    )
  );
}

/**
 * Implements hook_form().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @return array
 */
function owasp_login_velocity_check_form($form, &$form_state) {

  global $conf;

  if (!isset($conf['owasp_time'])) {
     variable_set('owasp_time',20);
  }

  if (!isset($conf['owasp_errormsg'])) {
     variable_set('owasp_errormsg',"You're attempting to login from a different geographic location than your recent session. As a security precaution, this login was blocked. Please try login at a later time.");
  }

  $form['owasp_time'] = array(
    '#type' => 'textfield',
    '#title' => 'OWASP login velocity check time interval (minutes)',
    '#size' => 60,
    '#default_value' => variable_get('owasp_time'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['owasp_errormsg'] = array(
    '#type' => 'textarea',
    '#title' => 'OWASP login velocity check error message',
    '#rows' => 5,
    '#default_value' => variable_get('owasp_errormsg'),
    '#required' => TRUE,
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Implements hook_submit().
 *
 * Additional submit handler for the login form.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function owasp_login_velocity_check_form_submit($form, &$form_state) {

		variable_set('owasp_time',$_REQUEST["owasp_time"]);
		variable_set('owasp_errormsg',$_REQUEST["owasp_errormsg"]);

		drupal_set_message("Saved.", 'status');
}

/**
 * Create a page for changing owasp time and error message
 *
 * @return array modified form array.
 */
function owasp_login_velocity_check_page() {

  return drupal_render(drupal_get_form('owasp_login_velocity_check_form'));

}

/**
 * Implements hook_form_alter.
 *
 * Launch additional handler during login.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 * @param $form_id
 *   String representing the name of the form itself. Typically this is the
 *   name of the function that generated the form.
 *
 */
function owasp_login_velocity_check_form_alter(&$form, $form_state, $form_id) {

  global $conf;

  switch ($form_id) {
    case 'user_login_block':
    case 'user_login' :

      if (!isset($conf['owasp_time'])) {
        variable_set('owasp_time',20);
      }

      if (!isset($conf['owasp_errormsg'])) {
        variable_set('owasp_errormsg',"You're attempting to login from a different geographic location than your recent session. As a security precaution, this login was blocked. Please try login at a later time.");
      }

      array_unshift($form['#validate'], 'custom_login_validate');
      break;
  }
}

/**
 * Checking owasp during sign-in
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function custom_login_validate($form, &$form_state) {

    // get uid and timezone for user by name
    $result = db_query("SELECT `uid`,`timezone` FROM `users` WHERE `name`='".addslashes($form_state['values']['name'])."' or `mail`='".addslashes($form_state['values']['name'])."'")->fetch();

    if(isset($result->uid))
    {

    	$uid = $result->uid;
    	$timezone = $result->timezone;

    	date_default_timezone_set($timezone);
    	$current_timestamp = time();

	// get IP address and time of the last known valid log-in
    	$result = db_query("SELECT `hostname`,`timestamp` from `watchdog` WHERE message='Session opened for %name.' and uid='$uid' order by `wid` DESC limit 0,1")->fetch();

        if(isset($result->hostname))
        {

    		$prior_ip = $result->hostname;
    		$prior_timestamp = $result->timestamp;

    		$current_ip = $_SERVER["REMOTE_ADDR"];

		$timediff = variable_get('owasp_time')*60;

		// checks if last valid log-in IP and current one do match during velocity check period
    		if($current_ip!=$prior_ip && $current_timestamp-$prior_timestamp<$timediff)
    		{

			$address = 'https://www.oss-usa.com/ip2location/ip2location.php?prior_ip='.$prior_ip.'&current_ip='.$current_ip;

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $address);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			$data = curl_exec($ch);
			curl_close($ch);

			list($country1,$country2) = explode("=",$data);

			// if IP from different countries then show error
        		if($country1!=$country2)form_set_error('name', t(variable_get('owasp_errormsg')));

    		}

	}

    }

}